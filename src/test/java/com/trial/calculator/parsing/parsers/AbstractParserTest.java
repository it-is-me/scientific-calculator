package com.trial.calculator.parsing.parsers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AbstractParserTest {

    private AbstractParser instance;

    @Before
    public void setUp() {
        instance = Mockito.mock(AbstractParser.class, Mockito.CALLS_REAL_METHODS);
    }


    @Test
    public void startsFromCharArgTrue() {
        // GIVEN
        char[] testData = new char[]{'a', 'b', '7', 'c', 'd'};

        // WHEN
        boolean a = instance.startsFrom(testData, 0, 'a');
        boolean c = instance.startsFrom(testData, 3, 'c');

        // THEN
        assertThat(a).isTrue();
        assertThat(c).isTrue();
    }

    @Test
    public void startsFromStringArgTrue() {
        // GIVEN
        char[] testData = new char[]{'a', 'b', '7', 'c', 'd'};

        // WHEN
        boolean ab = instance.startsFrom(testData, 0, "ab");
        boolean b7c = instance.startsFrom(testData, 1, "b7c");

        // THEN
        assertThat(ab).isTrue();
        assertThat(b7c).isTrue();
    }

    @Test
    public void startsFromCharArgFalse() {
        // GIVEN
        char[] testData = new char[]{'a', 'b', '7', 'c', 'd'};

        // WHEN
        boolean g = instance.startsFrom(testData, 0, 'g');
        boolean b = instance.startsFrom(testData, 2, 'b');

        // THEN
        assertThat(g).isFalse();
        assertThat(b).isFalse();
    }

    @Test
    public void startsFromStringArgFalse() {
        // GIVEN
        char[] testData = new char[]{'n', '3', 'c', '9', 'd'};

        // WHEN
        boolean ab = instance.startsFrom(testData, 0, "ab");
        boolean b7c = instance.startsFrom(testData, 1, "b7c");

        // THEN
        assertThat(ab).isFalse();
        assertThat(b7c).isFalse();
    }

    @Test
    public void readLetters() {
        // GIVEN
        String someTestData = "someTestData1234";
        char[] testDataChars = someTestData.toCharArray();

        // WHEN
        String result = instance.readLetters(testDataChars, 4, 4);

        // THEN
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("Test");
    }

    @Test
    public void canAdvanceTrue() {
        // GIVEN
        char[] testData = "someData".toCharArray();

        // WHEN
        boolean result = instance.canAdvance(testData, 7);

        // THEN
        assertThat(result).isTrue();
    }

    @Test
    public void canAdvanceFalse() {
        // GIVEN
        char[] testData = "someData".toCharArray();

        // WHEN
        boolean result = instance.canAdvance(testData, 8);

        // THEN
        assertThat(result).isFalse();
    }

    @Test
    public void resolveBracketsLengthPositive() {
        // GIVEN
        String brackets = "(1234(((567)9(0)(1)2)3-4)5)";
        String prefix = "ab123456(rc)ffc";
        String suffix = "123(1-3(345(12)gg)33)";
        char[] testData = (prefix + brackets + suffix).toCharArray();

        // WHEN
        int result = instance.resolveBracketsLength(testData, prefix.length());

        // THEN
        assertThat(result).isEqualTo(brackets.length());
    }

    @Test
    public void resolveBracketsLengthZeroCase() {
        // GIVEN
        String falseBrackets = "3(1234(((567)9(0)(1)2)3-4)5)";
        String prefix = "ab12356(rc)ffc";
        String suffix = "12(1-3(345(1)gg)33)";
        char[] falseBracketsChars = (prefix + falseBrackets + suffix).toCharArray();

        // WHEN
        int result = instance.resolveBracketsLength(falseBracketsChars, prefix.length());

        // THEN
        assertThat(result).isEqualTo(0);
    }
}