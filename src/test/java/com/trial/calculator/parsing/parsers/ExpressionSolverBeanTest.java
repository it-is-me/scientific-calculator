package com.trial.calculator.parsing.parsers;

import com.trial.calculator.parsing.abstracts.ExpressionSolver;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ExpressionSolverBeanTest {

    private static final Map<String, Double> TEST_DATA_CLEAR_BINDINGS = new HashMap<String, Double>(){{
        put("1+2*3+4", 11d);
        put("(1+2)*(3+4)", 21d);
        put("sqrt(2)*sqrt(2)", 2d);
        put("100+(100-50)*(10-5)", 350d);
        put("10+(90+(20+10)*30)", 1000d);
    }};

    private static final Map<String, Double> TEST_DATA_KEEP_BINDINGS = new LinkedHashMap<String, Double>(){{
        put("pi=3.14159265359", 3.14159265359);
        put("cos(pi)", -1d);
        put("id(someName)=sqrt(400)*50", 1000d);
        put("log(someName)", Math.log(1000d));
        put("cos(pi)*cos(pi)-cos(pi)", 2d);
        put("_+someName", 1002d);
    }};


    private Map<String, Double> bindings;

    private ExpressionSolver instance;

    @Before
    public void setUp() {
        bindings = new HashMap<>();
        instance = new ExpressionSolverBean(bindings);
    }

    @Test
    public void verifyAgainstTestDataClearBindings() {
        verifyTestData(TEST_DATA_CLEAR_BINDINGS, false);
    }

    @Test
    public void verifyAgainstTestDataKeepBindings() {
        verifyTestData(TEST_DATA_KEEP_BINDINGS, true);
    }

    public void verifyTestData(Map<String, Double> testData, boolean keepBinding) {
        for (Map.Entry<String, Double> testCase : testData.entrySet()) {
            if (!keepBinding) {
                bindings.clear();
            }
            // GIVEN
            String equation = testCase.getKey();
            double expectedAnswer = testCase.getValue();

            // WHEN
            double result = instance.resolveExpression(equation);

            // THEN
            try {
                verifyResult(expectedAnswer, result);
            } catch (Throwable e) {
                String exceptionString = String.format("Test failed for equation: %1$s! Expected %2$s, actual: %3$s",
                        equation, expectedAnswer, result);
                throw new RuntimeException(exceptionString, e);
            }

        }
    }

    private void verifyResult(double expected, double actualResult) {
        assertThat(actualResult).isCloseTo(expected, Offset.offset(0.01));
        assertThat(bindings.get("_")).isCloseTo(expected,  Offset.offset(0.01));
    }

}