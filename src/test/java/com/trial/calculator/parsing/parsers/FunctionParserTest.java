package com.trial.calculator.parsing.parsers;


import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import org.assertj.core.data.Offset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class FunctionParserTest {

    @Mock
    private Parser rootParser;

    @Mock
    private IndexRange indexRange;

    @Mock
    private Operand operand;

    @InjectMocks
    private FunctionParser instance;

    @Test
    public void canConsume() {
        // GIVEN
        String expr = "1234+sin(PI/2)+5678";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(5);
        given(indexRange.getLength()).willReturn(testData.length - 5);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(9);

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void canConsumeZero() {
        // GIVEN
        String expr = "1234+zin(PI/2)+5678";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(5);
        given(indexRange.getLength()).willReturn(testData.length - 5);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(0);

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parse() {
        // GIVEN
        String prefix = "1234+sin(";
        String suffix = ")+5678";
        String expr =  prefix + Math.PI/2 + suffix;
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(5);
        given(indexRange.getLength()).willReturn(testData.length - prefix.length());

        IndexRange expectedRequest =
                new IndexRange(
                        prefix.length() - 1,
                        testData.length - prefix.length() - suffix.length() + 2
                );

        given(operand.eval()).willReturn(Math.PI/2);

        given(rootParser.parse(eq(testData), eq(expectedRequest), eq(rootParser))).willReturn(operand);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        assertThat(resultValue).isCloseTo(Math.sin(Math.PI/2), Offset.offset(0.001));

        verify(rootParser).parse(testData, expectedRequest, rootParser);
        verifyNoMoreInteractions(rootParser);

    }



}