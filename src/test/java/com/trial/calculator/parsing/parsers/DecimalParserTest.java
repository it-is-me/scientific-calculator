package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DecimalParserTest {

    @Mock
    private Parser rootParser;

    @Mock
    private IndexRange indexRange;

    private DecimalParser instance;

    @Before
    public void setUp() {
        instance = new DecimalParser();
    }

    @Test
    public void canConsume() {
        // GIVEN
        int intValue = 1234;
        int fractionValue = 5678;
        String expr = "someData+" + intValue + "." + fractionValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(9);

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void canConsumeZero() {
        // GIVEN
        String expr = "someData+fdssfd";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(5);
        given(indexRange.getLength()).willReturn(testData.length - 5);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(0);

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parse() {
        // GIVEN
        int intValue = 1234;
        int fractionValue = 5678;
        String expr = "someData+" + intValue + "." + fractionValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        double expectedValue = Double.parseDouble(intValue + "." + fractionValue);
        assertThat(resultValue).isCloseTo(expectedValue, Offset.offset(0.0001d));

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parseStartsWithPoint() {
        // GIVEN
        int fractionValue = 5678;
        String expr = "someData+" + "." + fractionValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        double expectedValue = Double.parseDouble("." + fractionValue);
        assertThat(resultValue).isCloseTo(expectedValue, Offset.offset(0.0001d));

        verifyZeroInteractions(rootParser);
    }
}