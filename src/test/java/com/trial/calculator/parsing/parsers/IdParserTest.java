package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IdParserTest {

    private final String BINDING_NAME = "someName_1234";
    private final double BINDING_VALUE = 777d;

    @Mock
    private Map<String, Double> bindings;

    @Mock
    private Parser rootParser;

    @Mock
    private IndexRange indexRange;

    @InjectMocks
    private IdParser instance;

    @Before
    public void setUp() {
        when(bindings.get(BINDING_NAME)).thenReturn(BINDING_VALUE);
    }

    @Test
    public void canConsumePositiveResult() {
        // GIVEN
        String bindingName = "someValidBinding_321";
        char[] testData =  bindingName.toCharArray();

        given(indexRange.getStartIndex()).willReturn(0);
        given(indexRange.getLength()).willReturn(bindingName.length());

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(bindingName.length());

        verify(indexRange, times(2)).getStartIndex();

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void canConsumeWithBrackets() {
        // GIVEN
        String bindingName = "someValidBinding_321";
        char[] testData = ("id(" + bindingName + ")").toCharArray();

        given(indexRange.getStartIndex()).willReturn(0);
        given(indexRange.getLength()).willReturn(testData.length);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(testData.length);

        verify(indexRange, times(2)).getStartIndex();

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void canConsumeZeroResult() {
        // GIVEN
        String invalidBindingName = "121+omeinvalid243Binding_321";
        char[] invalidBindingData =  invalidBindingName.toCharArray();

        given(indexRange.getStartIndex()).willReturn(0);
        given(indexRange.getLength()).willReturn(invalidBindingName.length());

        // WHEN
        int result = instance.canConsume(invalidBindingData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(0);

        verifyZeroInteractions(rootParser);

        verify(indexRange, times(2)).getStartIndex();
    }

    @Test
    public void parse() {
        // GIVEN
        String bindingName = BINDING_NAME;
        char[] testData = ("1234+" + bindingName + "-5678").toCharArray();

        given(indexRange.getStartIndex()).willReturn(5);
        given(indexRange.getLength()).willReturn(testData.length);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double value = result.eval();

        // THEN
        assertThat(value).isCloseTo(BINDING_VALUE, Offset.offset(0.01d));

        verify(bindings).get(BINDING_NAME);
        verifyNoMoreInteractions(bindings);
    }

    @Test
    public void readIdName() {
        // GIVEN
        String bindingName = "someValidBinding_321";
        char[] testData =  ("12345" + bindingName).toCharArray();

        given(indexRange.getStartIndex()).willReturn(0);
        given(indexRange.getLength()).willReturn(testData.length);

        // WHEN
        String result = instance.readIdName(testData, 5);

        // THEN
        assertThat(result).isEqualTo(bindingName);

    }

    @Test
    public void readIdNameWithBrackets() {
        // GIVEN
        String bindingName = "someValidBinding_321";
        char[] testData =  ("1234+id(" + bindingName + ")").toCharArray();

        given(indexRange.getStartIndex()).willReturn(0);
        given(indexRange.getLength()).willReturn(testData.length);

        // WHEN
        String result = instance.readIdName(testData, 5);

        // THEN
        assertThat(result).isEqualTo(bindingName);

    }

    @Test
    public void getIdNameOffsetWithId() {
        // GIVEN
        String bindingNameWithId = "1234id(name_)";
        char[] testData = bindingNameWithId.toCharArray();

        // WHEN
        int result = instance.getIdNameOffset(testData, 4);

        // THEN
        assertThat(result).isEqualTo(3);

    }

    @Test
    public void getIdNameOffsetWithoutId() {
        // GIVEN
        String bindingNameWithoutId = "1234+name_";
        char[] testData = bindingNameWithoutId.toCharArray();

        // WHEN
        int result = instance.getIdNameOffset(testData, 5);

        // THEN
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void startsFromIdPrefixFalse() {
        // GIVEN
        String bindingNameWithoutId = "1234+name_";
        char[] testData = bindingNameWithoutId.toCharArray();

        // WHEN
        boolean result = instance.startsFromIdPrefix(testData, 5);

        // THEN
        assertThat(result).isFalse();
    }

    @Test
    public void startsFromIdPrefixTrue() {
        // GIVEN
        String bindingNameWithId = "1234id(name_)";
        char[] testData = bindingNameWithId.toCharArray();

        // WHEN
        boolean result = instance.startsFromIdPrefix(testData, 4);

        // THEN
        assertThat(result).isTrue();
    }
}