package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import org.assertj.core.data.Offset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class IntegerParserTest {

    @Mock
    private Parser rootParser;

    @Mock
    private IndexRange indexRange;

    @InjectMocks
    private IntegerParser instance;

    @Test
    public void canConsume() {
        // GIVEN
        int value = 1234;
        String expr = "someData+" + value + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        int result = instance.canConsume(testData, indexRange, rootParser);

        // THEN
        assertThat(result).isEqualTo(4);

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parse() {
        // GIVEN
        int value = 12345;
        String expr = "someData+" + value + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(5);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        assertThat(resultValue).isCloseTo(value, Offset.offset(0.01d));

        verifyZeroInteractions(rootParser);
    }

}