package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class NumberParserTest {

    @Mock
    private Parser rootParser;

    @Mock
    private IndexRange indexRange;

    private NumberParser instance;

    @Before
    public void setUp() {
        instance = new NumberParser();
    }

    @Test
    public void parseDecimal() {
        // GIVEN
        int intValue = 1234;
        int fractionValue = 5678;
        String expr = "someData+" + intValue + "." + fractionValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        double expectedValue = Double.parseDouble(intValue + "." + fractionValue);
        assertThat(resultValue).isCloseTo(expectedValue, Offset.offset(0.001d));

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parseDecimalStartsWithPoint() {
        // GIVEN
        int fractionValue = 567;
        String expr = "someData+" + "." + fractionValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 9);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        double expectedValue = Double.parseDouble("." + fractionValue);
        assertThat(resultValue).isCloseTo(expectedValue, Offset.offset(0.001d));

        verifyZeroInteractions(rootParser);
    }

    @Test
    public void parseInteger() {
        // GIVEN
        int intValue = 1234;
        String expr = "someData+" + intValue + "+anotherData";
        char[] testData = expr.toCharArray();

        given(indexRange.getStartIndex()).willReturn(9);
        given(indexRange.getLength()).willReturn(testData.length - 4);

        // WHEN
        Operand result = instance.parse(testData, indexRange, rootParser);
        double resultValue = result.eval();

        // THEN
        assertThat(resultValue).isCloseTo(intValue, Offset.offset(0.001d));

        verifyZeroInteractions(rootParser);
    }

}