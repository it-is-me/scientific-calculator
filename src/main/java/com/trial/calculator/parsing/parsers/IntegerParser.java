package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.expressions.abstracts.SimpleOperand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

public class IntegerParser extends AbstractParser {

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        int cursor = fromRange.getStartIndex();
        while(canAdvance(expression, cursor)) {
            if (!Character.isDigit(expression[cursor])) {
                break;
            }
            cursor++;
        }
        return cursor - fromRange.getStartIndex();
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        Integer value = Integer.parseInt(String.copyValueOf(expression,
                fromRange.getStartIndex(),
                fromRange.getLength()));
        return new SimpleOperand(value);
    }

}
