package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.expressions.terms.Id;
import com.trial.calculator.parsing.abstracts.Parser;
import com.trial.calculator.parsing.abstracts.IndexRange;

import java.util.Map;

public class IdParser extends AbstractParser implements Parser {

    private static final String ID_PREFIX = "id(";

    private final Map<String, Double> bindings;

    public IdParser(Map<String, Double> bindings) {
        this.bindings = bindings;
    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        int extraSymbolsCount = 0;
        if (startsFromIdPrefix(expression, fromRange.getStartIndex())) {
            extraSymbolsCount += ID_PREFIX.length() + 1;
        }
        return readIdName(expression, fromRange.getStartIndex()).length() + extraSymbolsCount;
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        return new Id(readIdName(expression, fromRange.getStartIndex()), bindings);
    }

    String readIdName(char[] expression, int startFrom) {
        int nameStartIndex = startFrom + getIdNameOffset(expression, startFrom);
        int cursor = nameStartIndex;
        int consumedLength = 0;
        if (canAdvance(expression, cursor)) {
            char c;
            outer : while(canAdvance(expression, cursor)) {
                c = expression[cursor];

                if (Character.isAlphabetic(c)) {
                    consumedLength++;
                    cursor++;
                    continue;
                }
                if (c == '_') {
                    consumedLength++;
                    cursor++;

                    while(canAdvance(expression, cursor)) {
                        if (Character.isDigit(expression[cursor])) {
                            consumedLength++;
                            cursor++;
                        } else {
                            break outer;
                        }
                    }
                }
                break;
            }
        }
        return readLetters(expression, nameStartIndex, consumedLength);
    }

    int getIdNameOffset(char[] expression, int fromSymbolIncluding) {
        if (startsFromIdPrefix(expression, fromSymbolIncluding)) {
            return ID_PREFIX.length();
        } else {
            return 0;
        }
    }

    boolean startsFromIdPrefix(char[] expression, int fromSymbolIncluding) {
        return startsFrom(expression, fromSymbolIncluding, ID_PREFIX);
    }

}
