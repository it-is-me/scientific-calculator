package com.trial.calculator.parsing.parsers;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class FunctionParser extends UniversalFunctionParser {

    private static final Map<String, Function<Double, Double>> SUPPORTED_FUNCTIONS;

    static {
        SUPPORTED_FUNCTIONS = new HashMap<>();
        SUPPORTED_FUNCTIONS.put("sin", Math::sin);
        SUPPORTED_FUNCTIONS.put("cos", Math::cos);
        SUPPORTED_FUNCTIONS.put("sqrt", Math::sqrt);
        SUPPORTED_FUNCTIONS.put("log", Math::log);
    }

    public FunctionParser() {
        super(SUPPORTED_FUNCTIONS);
    }

}
