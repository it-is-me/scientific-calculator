package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

public class BracketsParser extends AbstractParser {

    public BracketsParser() {

    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        if (!canAdvance(expression, fromRange.getStartIndex()) || expression[fromRange.getStartIndex()] != '(') {
            return 0;
        }
        return resolveBracketsLength(expression, fromRange.getStartIndex());
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        return rootParser.parse(expression,
                new IndexRange(fromRange.getStartIndex() + 1,
                        fromRange.getLength() - 2), rootParser);
    }

}
