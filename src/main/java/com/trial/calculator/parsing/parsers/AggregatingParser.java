package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

public abstract class AggregatingParser extends AbstractParser {

    private final Parser[] prioritisedParsers;

    public AggregatingParser(Parser... prioritisedParsers) {
        this.prioritisedParsers = prioritisedParsers;
    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        int canConsume = 0;
        for (Parser parser : prioritisedParsers) {
            canConsume = parser.canConsume(expression, fromRange, rootParser);
            if (canConsume > 0) {
                return canConsume;
            }
        }
        return canConsume;
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        int canConsume;
        for (Parser parser : prioritisedParsers) {
            canConsume = parser.canConsume(expression, fromRange, rootParser);
            if (canConsume > 0) {
                return parser.parse(
                        expression,
                        new IndexRange(fromRange.getStartIndex(), canConsume),
                        rootParser
                );
            }
        }
        throw new RuntimeException(
                String.format(
                        "Unknown data type starting at %1$s, cannot parse expression.",
                        fromRange.getStartIndex()
                ));
    }
}
