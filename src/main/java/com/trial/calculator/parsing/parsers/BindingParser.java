package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.expressions.terms.Binding;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

import java.util.Map;

public class BindingParser extends AbstractParser {

    private final IdParser idParser;
    private final Map<String, Double> bindings;

    public BindingParser(Map<String, Double> bindings) {
        idParser = new IdParser(bindings);
        this.bindings = bindings;
    }

    BindingParser(IdParser idParser, Map<String, Double> bindings) {
        this.idParser = idParser;
        this.bindings = bindings;
    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        int idLength = idParser.canConsume(expression, fromRange, rootParser);
        if (idLength > 0 && canAdvance(expression, fromRange.getStartIndex() + idLength)) {
            return expression[fromRange.getStartIndex() + idLength] == '=' ? fromRange.getLength() : 0;
        }
        return 0;
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        int idLength = idParser.canConsume(expression, fromRange, rootParser);
        Operand rightExpression = rootParser.parse(expression,
                new IndexRange(
                        fromRange.getStartIndex() + idLength + 1,
                        fromRange.getLength() - idLength - 1
                ),
                rootParser);
        return new Binding(idParser.readIdName(expression, fromRange.getStartIndex()), rightExpression, bindings);
    }

}
