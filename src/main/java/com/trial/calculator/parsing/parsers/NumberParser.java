package com.trial.calculator.parsing.parsers;

public class NumberParser extends AggregatingParser {

    /**
     * Default constructor
     */
    public NumberParser() {
        super(new DecimalParser(), new IntegerParser());
    }

}
