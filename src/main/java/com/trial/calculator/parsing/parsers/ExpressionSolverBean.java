package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.BiFunctionHolder;
import com.trial.calculator.parsing.abstracts.ExpressionSolver;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpressionSolverBean extends AbstractParser implements ExpressionSolver {

    private static final int DEFAULT_OPERATIONS_ARRAY_SIZE = 30;

    private final BiFunctionHolder functionProvider;
    private final Parser termParser;
    private final Map<String, Double> bindings;

    /**
     * Default constructor
     * @param bindings expression bindings holder
     */
    public ExpressionSolverBean(Map<String, Double> bindings) {
        functionProvider = new BiFunctionHolder();
        termParser = new TermParser(bindings);
        this.bindings = bindings;
    }

    /*
        Constructor only for tests
     */
    ExpressionSolverBean(BiFunctionHolder functionProvider, Parser termParser, Map<String, Double> bindings) {
        this.functionProvider = functionProvider;
        this.termParser = termParser;
        this.bindings = bindings;
    }


    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        return expression.length - fromRange.getStartIndex() - 1;
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        List<Operand> terms = new ArrayList<>(DEFAULT_OPERATIONS_ARRAY_SIZE);
        List<Character> functionSigns = new ArrayList<>(DEFAULT_OPERATIONS_ARRAY_SIZE);
        int cursor = fromRange.getStartIndex();
        int termLength;
        int remainingLength;

        // Adding all the terms and operations
        while (cursor <= fromRange.getStartIndex() + fromRange.getLength()) {
            remainingLength = fromRange.getLength() - (cursor - fromRange.getStartIndex());
            termLength = readTermLength(expression, new IndexRange(cursor, remainingLength));

            terms.add(termParser.parse(
                    expression,
                    new IndexRange(
                            cursor,
                            termLength
                    ),
                    this
            ));

            if (canAdvance(expression, cursor + termLength)
                    && cursor + termLength < fromRange.getStartIndex() + fromRange.getLength()) {
                functionSigns.add(expression[cursor + termLength]);
            }

            cursor++;
            cursor+=termLength;
        }

        return resolveEquation(terms, functionSigns);

    }

    Operand resolveEquation(List<Operand> terms,
                            List<Character> functionSigns) {
        while(functionSigns.size() > 0) {
            mergeLastPossible(terms, functionSigns);
        }
        return terms.get(0);
    }

    void mergeLastPossible(List<Operand> terms,
                           List<Character> functionSigns) {
        int mergePosition = findLastMergingIndex(functionSigns);
        Operand mergeResult = functionProvider.getAsFunction(
                functionSigns.get(mergePosition),
                terms.get(mergePosition),
                terms.get(mergePosition + 1)
        );
        terms.remove(mergePosition + 1);
        terms.remove(mergePosition);
        functionSigns.remove(mergePosition);
        terms.add(mergePosition, mergeResult);
    }

    int findLastMergingIndex(List<Character> functionSigns) {
        int lastIndex = functionSigns.size() - 1;
        if (lastIndex == 0) {
            return 0;
        }
        int lastPriority;
        int currentPriority;

        while(lastIndex > 0) {
            lastPriority = functionProvider.getFunctionPriority(functionSigns.get(lastIndex));
            currentPriority = functionProvider.getFunctionPriority(functionSigns.get(lastIndex - 1));
            if (currentPriority >= lastPriority) {
                return lastIndex;
            } else {
                lastIndex--;
            }
        }
        return lastIndex;
    }

    int readTermLength(char[] expression, IndexRange fromRange) {
        return termParser.canConsume(expression, fromRange, this);
    }

    @Override
    public double resolveExpression(String expression) {

        char[] preparedInput = expression.replace(" ", "").toCharArray();

        Operand preparedEquation = parse(
                preparedInput,
                new IndexRange(0, preparedInput.length - 1),
                this);

        double result = preparedEquation.eval();
        bindings.put("_", result);
        return result;
    }

}
