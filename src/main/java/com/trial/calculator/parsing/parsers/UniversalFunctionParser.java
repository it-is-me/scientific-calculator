package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;
import com.trial.calculator.parsing.abstracts.UniversalFunction;

import java.util.Map;
import java.util.function.Function;

public class UniversalFunctionParser extends AbstractParser {

    private static final Character FUNCTION_NAME_DELIMITER = '(';

    private final Map<String, Function<Double, Double>> functions;

    public UniversalFunctionParser(Map<String, Function<Double, Double>> functions) {
        this.functions = functions;
    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        String nameCandidate = readFunctionName(expression, fromRange);
        int expectedDelimiterPosition = fromRange.getStartIndex() + nameCandidate.length();
        if (!functions.containsKey(nameCandidate) ||
                !canAdvance(expression, expectedDelimiterPosition) ||
                expression[expectedDelimiterPosition] != FUNCTION_NAME_DELIMITER) {
            return 0;
        }
        return nameCandidate.length() +
                resolveBracketsLength(expression,
                        fromRange.getStartIndex() + nameCandidate.length());
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        String functionName = readFunctionName(expression, fromRange);
        Function<Double, Double> function = functions.get(functionName);
        int openingBracketIndex = fromRange.getStartIndex() + functionName.length();
        int bracketsLength = resolveBracketsLength(expression, openingBracketIndex);
        Operand functionArgument =
                rootParser.parse(expression,
                        new IndexRange(openingBracketIndex, bracketsLength),
                        rootParser);
        return new UniversalFunction(function, functionArgument);
    }

    String readFunctionName(char[] expression, IndexRange fromRange) {
        int cursor = fromRange.getStartIndex();
        while (canAdvance(expression, cursor)) {
            if (!Character.isAlphabetic(expression[cursor])) {
                break;
            }
            cursor++;
        }
        return readLetters(expression, fromRange.getStartIndex(), cursor - fromRange.getStartIndex());
    }
}
