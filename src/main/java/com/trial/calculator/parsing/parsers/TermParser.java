package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.expressions.terms.Negate;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

import java.util.Map;

public class TermParser extends AggregatingParser {

    /**
     * Default constructor
     * @param bindings bindings holder ID_NAME : VALUE
     */
    public TermParser(Map<String, Double> bindings) {
        super(
                new BindingParser(bindings),
                new BracketsParser(),
                new FunctionParser(),
                new IdParser(bindings),
                new NumberParser()
        );
    }

    @Override
    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        if (startsFromMinus(expression, fromRange)) {
            return 1 + super.canConsume(
                    expression,
                    new IndexRange(fromRange.getStartIndex() + 1, fromRange.getLength() - 1),
                    rootParser
            );
        }
        return super.canConsume(expression, fromRange, rootParser);
    }

    @Override
    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        if (startsFromMinus(expression, fromRange)) {
            return new Negate(super.parse(
                    expression,
                    new IndexRange(fromRange.getStartIndex() + 1, fromRange.getLength() - 1),
                    rootParser
            ));
        }
        return super.parse(expression, fromRange, rootParser);
    }

    boolean startsFromMinus(char[] expression, IndexRange fromRange) {
        return startsFrom(expression, fromRange.getStartIndex(), '-');
    }

}
