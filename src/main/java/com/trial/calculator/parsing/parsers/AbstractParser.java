package com.trial.calculator.parsing.parsers;

import com.trial.calculator.parsing.abstracts.Parser;

public abstract class AbstractParser implements Parser {

    boolean startsFrom(char[] expression, int firstSymbolIndex, String expect) {
        if (!canAdvance(expression, firstSymbolIndex + expect.length() - 1)) {
            return false;
        }
        return expect.equals(readLetters(expression, firstSymbolIndex, expect.length()));
    }

    boolean startsFrom(char[] expression, int firstSymbolIndex, char expect) {
        return canAdvance(expression, firstSymbolIndex)
                && expression[firstSymbolIndex] == expect;
    }

    String readLetters(char[] expression, int fromIncluding, int count) {
        return String.copyValueOf(expression, fromIncluding, count);
    }

    boolean canAdvance(char[] expression, int toSymbolWithIndex) {
        return expression.length > toSymbolWithIndex;
    }

    int resolveBracketsLength(char[] expression, int openingBracketIndex) {
        if (canAdvance(expression, openingBracketIndex)
                && expression[openingBracketIndex] == '(') {
            int bracketsScore = -1;
            int cursor = openingBracketIndex + 1;
            int consumable = 1;
            while (canAdvance(expression, cursor)) {
                if (expression[cursor] == '(') {
                    bracketsScore--;
                } else if (expression[cursor] == ')') {
                    bracketsScore++;
                }
                consumable++;
                cursor++;
                if (bracketsScore == 0) {
                    return consumable;
                }
            }
            throw new RuntimeException("Incorrect expression! Not enough closing brackets!");
        } else {
            return 0;
        }
    }

}
