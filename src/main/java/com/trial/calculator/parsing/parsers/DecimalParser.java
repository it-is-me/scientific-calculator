package com.trial.calculator.parsing.parsers;

import com.trial.calculator.expressions.abstracts.Operand;
import com.trial.calculator.expressions.abstracts.SimpleOperand;
import com.trial.calculator.parsing.abstracts.IndexRange;
import com.trial.calculator.parsing.abstracts.Parser;

public class DecimalParser extends AbstractParser {

    private final IntegerParser integerParser;

    public DecimalParser() {
        this.integerParser = new IntegerParser();
    }

    public int canConsume(char[] expression, IndexRange fromRange, Parser rootParser) {
        if (startsWithPoint(expression, fromRange)) {
            // adding 1 to count point itself
            return 1 + integerParser.canConsume(
                    expression,
                    new IndexRange(fromRange.getStartIndex() + 1, fromRange.getLength()),
                    rootParser
                    );
        }
        int intPartLength = integerParser.canConsume(expression, fromRange, rootParser);
        if (intPartLength == 0) return 0;

        if (!canAdvance(expression, fromRange.getStartIndex() + intPartLength)
                || expression[fromRange.getStartIndex() + intPartLength] != '.') {
            // no fraction part - not a decimal
            return 0;
        }

        int decimalPartLength = integerParser.canConsume(
                expression,
                new IndexRange(fromRange.getStartIndex() + intPartLength + 1,
                        fromRange.getLength() - intPartLength - 1),
                rootParser);

        return intPartLength + 1 + decimalPartLength;
    }

    public Operand parse(char[] expression, IndexRange fromRange, Parser rootParser) {
        int length = canConsume(expression, fromRange, rootParser);
        double value = Double
                .parseDouble(
                        readLetters(expression, fromRange.getStartIndex(), length));
        return new SimpleOperand(value);
    }

    private boolean startsWithPoint(char[] expression, IndexRange fromRange) {
        return canAdvance(expression, fromRange.getStartIndex()) && expression[fromRange.getStartIndex()] == '.';
    }
}
