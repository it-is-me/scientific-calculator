package com.trial.calculator.parsing.abstracts;

import com.trial.calculator.expressions.abstracts.Operand;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class BiFunctionHolder {

    private final Map<Character, BiFunction<Double, Double, Double>> functionsBySign;
    private final Map<Character, Integer> functionPriorityBySign;

    /**
     * Default constructor
     */
    public BiFunctionHolder() {
        functionsBySign = new HashMap<>();
        functionsBySign.put('*', (a, b) -> a * b);
        functionsBySign.put('/', (a, b) -> a / b);
        functionsBySign.put('+', (a, b) -> a + b);
        functionsBySign.put('-', (a, b) -> a - b);

        functionPriorityBySign = new HashMap<>();
        functionPriorityBySign.put('*', 0);
        functionPriorityBySign.put('/', 0);
        functionPriorityBySign.put('+', 1);
        functionPriorityBySign.put('-', 1);
    }

    public Operand getAsFunction(char functionSign, Operand firstOperand, Operand secondOperand) {
        if (!functionsBySign.containsKey(functionSign)) {
            throw new RuntimeException("Unknown function: " + functionSign);
        }
        return new UniversalBiFunction(functionsBySign.get(functionSign), firstOperand, secondOperand);
    }

    public int getFunctionPriority(char functionSign) {
        if (!functionPriorityBySign.containsKey(functionSign)) {
            throw new RuntimeException("Unknown function: " + functionSign);
        }
        return functionPriorityBySign.get(functionSign);
    }

}
