package com.trial.calculator.parsing.abstracts;

import com.trial.calculator.expressions.abstracts.Operand;

import java.util.function.BiFunction;

public class UniversalBiFunction implements Operand {

    private final BiFunction<Double, Double, Double> executor;
    private final Operand firstArgument;
    private final Operand secondArgument;

    public UniversalBiFunction(BiFunction<Double, Double, Double> executor,
                               Operand firstArgument,
                               Operand secondArgument) {
        this.executor = executor;
        this.firstArgument = firstArgument;
        this.secondArgument = secondArgument;
    }

    @Override
    public double eval() {
        return executor.apply(firstArgument.eval(), secondArgument.eval());
    }

}
