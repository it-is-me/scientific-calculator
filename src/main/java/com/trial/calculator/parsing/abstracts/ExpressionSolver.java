package com.trial.calculator.parsing.abstracts;

public interface ExpressionSolver {

    double resolveExpression(String expression);

}
