package com.trial.calculator.parsing.abstracts;

import java.util.Objects;

public class IndexRange {

    private final int startIndex;
    private final int length;

    public IndexRange(int startIndex, int length) {
        this.startIndex = startIndex;
        this.length = length;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexRange that = (IndexRange) o;
        return startIndex == that.startIndex &&
                length == that.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startIndex, length);
    }

}
