package com.trial.calculator.parsing.abstracts;

import com.trial.calculator.expressions.abstracts.Operand;

public interface Parser {

    int canConsume(char[] expression, IndexRange fromRange, Parser rootParser);

    Operand parse(char[] expression, IndexRange fromRange, Parser rootParser);

}
