package com.trial.calculator.parsing.abstracts;

import com.trial.calculator.expressions.abstracts.Operand;

import java.util.function.Function;

public class UniversalFunction implements Operand {

    private final Function<Double, Double> executor;
    private final Operand argument;

    public UniversalFunction(Function<Double, Double> executor, Operand argument) {
        this.executor = executor;
        this.argument = argument;
    }

    @Override
    public double eval() {
        return executor.apply(argument.eval());
    }

}
