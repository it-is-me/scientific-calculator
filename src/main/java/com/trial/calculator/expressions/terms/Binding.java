package com.trial.calculator.expressions.terms;

import com.trial.calculator.expressions.abstracts.Expression;
import com.trial.calculator.expressions.abstracts.Operand;

import java.util.Map;

public class Binding extends Expression {

    public Binding(String name, Operand operand, Map<String, Double> bindings) {
        super(() -> {
            double value = operand.eval();
            bindings.put(name, value);
            return value;
        });
    }

    @Override
    public double eval() {
        return operand.eval();
    }

}
