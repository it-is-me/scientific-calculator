package com.trial.calculator.expressions.terms;

import com.trial.calculator.expressions.abstracts.OneOperandExpression;
import com.trial.calculator.expressions.abstracts.Operand;

public class Negate extends OneOperandExpression {

    public Negate(Operand operand) {
        super(operand);
    }

    @Override
    public double eval() {
        return - operand.eval();
    }

}
