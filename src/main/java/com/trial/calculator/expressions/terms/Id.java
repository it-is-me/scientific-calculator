package com.trial.calculator.expressions.terms;

import com.trial.calculator.expressions.abstracts.Expression;

import java.util.Map;

public class Id extends Expression {

    public Id(String name, Map<String, Double> bindings) {
        super(() -> bindings.get(name));
    }

    @Override
    public double eval() {
        return operand.eval();
    }

}
