package com.trial.calculator.expressions.abstracts;

public class SimpleOperand implements Operand {

    private final double value;

    public SimpleOperand(double value) {
        this.value = value;
    }

    @Override
    public double eval() {
        return value;
    }
    
}
