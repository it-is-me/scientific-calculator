package com.trial.calculator.expressions.abstracts;

public abstract class Expression implements Operand {

    protected final Operand operand;

    public Expression(Operand operand) {
        this.operand = operand;
    }

}
