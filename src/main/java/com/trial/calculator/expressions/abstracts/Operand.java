package com.trial.calculator.expressions.abstracts;

public interface Operand {

    double eval();

}
