package com.trial.calculator.expressions.abstracts;

public class OneOperandExpression extends Expression {

    public OneOperandExpression(Operand operand) {
        super(operand);
    }

    @Override
    public double eval() {
        return operand.eval();
    }

}
